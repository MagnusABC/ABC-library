# ABC-library 

Abc is a library of C-functions for building finite element applications. 
The library is split on these tar-files:
A) Include files and the ABC-library (libABC.a) for Linux X86_64.
B) The source code under GNU-lincence.
C) The source code for the plotting software Post4 under GNU-licence.
X) Pieces of external software such as lapack. This part does not belong to the ABC-library.
